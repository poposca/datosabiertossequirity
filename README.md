# Seguridad del Ecuador

Página web que muestra visualizaciones realizadas con datos del portal [datosabiertos](datosabiertos.gob.ec) del gobierno del Ecuador.

Se muestra información concerniente al año 2021 en temas como armas decomisadas, personas detenidas, desaparecidas, etc.
